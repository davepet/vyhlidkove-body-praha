//
//  TabBarController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 24/02/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import UIKit
import UserNotifications

class TabBarController: UITabBarController {
    
    var databaseVC : UIViewController = DatabaseViewController()
    var mapVC : UIViewController = MapViewController()
    var vyletyVC : UIViewController = PlanovaneVyletyTableViewController()
    var profileVC : UIViewController = ProfileViewController()
    
    var vylety = [Vylety]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let databaseVCTabBarItem : UITabBarItem = UITabBarItem(title: "Vyhlídková místa", image: UIImage(systemName: "list.dash"), selectedImage: UIImage(systemName: "list.dash"))
        let mapVCTabBarItem : UITabBarItem = UITabBarItem(title: "Mapa", image: UIImage(systemName: "map.fill"), selectedImage: UIImage(systemName: "map.fill"))
        let vyletyVCTabBarItem : UITabBarItem = UITabBarItem(title: "Plánované výlety", image: UIImage(systemName: "flag"), selectedImage: UIImage(systemName: "flag"))
        let profileVCTabBarItem : UITabBarItem = UITabBarItem(title: "Profil", image: UIImage(systemName: "person.circle"), selectedImage: UIImage(systemName: "person.circle"))
        
        databaseVC.tabBarItem = databaseVCTabBarItem
        mapVC.tabBarItem = mapVCTabBarItem
        vyletyVC.tabBarItem = vyletyVCTabBarItem
        profileVC.tabBarItem = profileVCTabBarItem
        
        tabBarController?.tabBar.tintColor = UIColor(red: 21, green: 152, blue: 10, alpha: 1)
        tabBarController?.tabBar.unselectedItemTintColor = UIColor.systemGray
        
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if granted {
                DispatchQueue.main.async {
                    self.sendNoTripNotification()
                }
            } else {
                print("not granted permission")
            }
        }
    }
    
    func sendNoTripNotification() {
        let center = UNUserNotificationCenter.current()

        if vylety.count == 0 {
            let noTripNotification = UNMutableNotificationContent()
            noTripNotification.title = "Nemáte žádný nadcházející výlet"
            noTripNotification.body = "Naplánujte si další výlet na zajímavé místo"
            noTripNotification.badge = NSNumber(value: UIApplication.shared.applicationIconBadgeNumber + 1)
            noTripNotification.sound = UNNotificationSound.default
            
            var dateComonent = DateComponents()
            dateComonent.calendar = Calendar.current
            dateComonent.weekday = 5
            dateComonent.hour = 15
            dateComonent.minute = 0
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComonent, repeats: true)
            let request = UNNotificationRequest(identifier: "NoTrip", content: noTripNotification, trigger: trigger)
            
            center.add(request) { (error : Error?) in
                if let theError = error {
                    print(theError.localizedDescription)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let initialVC = self.storyboard?.instantiateViewController(identifier: "initialVC") as! TabBarController
        let navigationController = UINavigationController(rootViewController: initialVC)
        navigationController.modalPresentationStyle = .fullScreen
        
        if UserDefaults.standard.object(forKey: "vyhlidky") != nil {
            self.show(navigationController, sender: self)
        } else {
            if Reachability.isConnectedToNetwork() {
                self.show(navigationController, sender: self)
            } else {
                let offlineVC = self.storyboard?.instantiateViewController(identifier: "OfflineViewController") as! OfflineViewController
                let navigationController = UINavigationController(rootViewController: offlineVC)
                navigationController.modalPresentationStyle = .fullScreen
                self.show(navigationController, sender: self)
            }
        }
        
        if UserDefaults.standard.data(forKey: "vylety") != nil {
            if let loadedVylety = UserDefaults.standard.data(forKey: "vylety") {
                let decoded = try! JSONDecoder().decode([Vylety].self, from: loadedVylety)
                vylety = decoded
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
