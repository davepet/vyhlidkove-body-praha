//
//  ProfileViewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 02/04/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

class ProfileViewController: UIViewController {
    
    var navstevy = [Navstevy]()
    
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var achieve1: UIImageView!
    @IBOutlet weak var achieve5: UIImageView!
    @IBOutlet weak var achieve10: UIImageView!
    @IBOutlet weak var achieve25: UIImageView!
    @IBOutlet weak var achieve50: UIImageView!
    @IBOutlet weak var achieve100: UIImageView!
    @IBOutlet weak var achieve200: UIImageView!
    @IBOutlet weak var achieve300: UIImageView!
    @IBOutlet weak var achieve323: UIImageView!
    //TODO: user login/signup (via gmail, fb,...)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        achieve1.image = UIImage(named: "achieveHuh")
        achieve5.image = UIImage(named: "achieveHuh")
        achieve10.image = UIImage(named: "achieveHuh")
        achieve25.image = UIImage(named: "achieveHuh")
        achieve50.image = UIImage(named: "achieveHuh")
        achieve100.image = UIImage(named: "achieveHuh")
        achieve200.image = UIImage(named: "achieveHuh")
        achieve300.image = UIImage(named: "achieveHuh")
        achieve323.image = UIImage(named: "achieveHuh")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.data(forKey: "navstevy") != nil {
            if let loadedNavstevy = UserDefaults.standard.data(forKey: "navstevy") {
                let decoded = try! JSONDecoder().decode([Navstevy].self, from: loadedNavstevy)
                navstevy = decoded
            }
        }
        print(navstevy)
        //TODO: labels for achieves... may be clickable with text showing up
        //TODO: displaying unlocked all achieves atm. change >= to > ?? but make sure when 5 places visited, it displays unlocked 1 and 5 badge
        if navstevy.count > 0 && navstevy.count < 4 {
            achieve1.image = UIImage(named: "achieveOne")
        } else if navstevy.count > 4 && navstevy.count < 9 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
        } else if navstevy.count > 9 && navstevy.count < 24 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
        } else if navstevy.count > 24 && navstevy.count < 49 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
        } else if navstevy.count > 49 && navstevy.count < 99 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
            achieve50.image = UIImage(named: "achieveFifty")
        } else if navstevy.count > 99 && navstevy.count < 199 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
            achieve50.image = UIImage(named: "achieveFifty")
            achieve100.image = UIImage(named: "achieveHundred")
        } else if navstevy.count > 199 && navstevy.count < 299 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
            achieve50.image = UIImage(named: "achieveFifty")
            achieve100.image = UIImage(named: "achieveHundred")
            achieve200.image = UIImage(named: "achieveTwohundred")
        } else if navstevy.count > 299 && navstevy.count < 323 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
            achieve50.image = UIImage(named: "achieveFifty")
            achieve100.image = UIImage(named: "achieveHundred")
            achieve200.image = UIImage(named: "achieveTwohundred")
            achieve300.image = UIImage(named: "achieveThreehundred")
        } else if navstevy.count == 323 {
            achieve1.image = UIImage(named: "achieveOne")
            achieve5.image = UIImage(named: "achieveFive")
            achieve10.image = UIImage(named: "achieveTen")
            achieve25.image = UIImage(named: "achieveTwentyfive")
            achieve50.image = UIImage(named: "achieveFifty")
            achieve100.image = UIImage(named: "achieveHundred")
            achieve200.image = UIImage(named: "achieveTwohundred")
            achieve300.image = UIImage(named: "achieveThreehundred")
            achieve323.image = UIImage(named: "achieveAll")
        }
    }
}
