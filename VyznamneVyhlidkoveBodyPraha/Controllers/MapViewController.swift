//
//  MapViewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 24/02/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    var vyhlidky : Vyhlidky = Vyhlidky(type: "", name: "", features: [])
    let apiCall = API()
    fileprivate let locationManager = CLLocationManager()
    var userTracker : MKUserTrackingButton?
    var compass : MKCompassButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        checkLocationServices()
        
        setUpUserLocationButton()
        
        apiCall.fetchData() { (vysledneVyhlidky)  in
            self.vyhlidky = vysledneVyhlidky
            DispatchQueue.main.async {
                self.fetchVyhlidkyOnMap(self.vyhlidky)
            }
        }
    }
    
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            checkLocationAuthorization()
        } else {
            deniedLocationPermission()
        }
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
        case .denied:
            deniedLocationPermission()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            mapView.showsUserLocation = true
        case .restricted:
            break
        case .authorizedAlways:
            break
        @unknown default:
            fatalError()
        }
    }
    
    func deniedLocationPermission() {
        let alertMapPermissionDenied = UIAlertController(title: "Nejsou povoleny lokalizační služby.", message: "Povolte prosím lokalizační služby v nastevní.", preferredStyle: UIAlertController.Style.alert)
        let settingsAction = UIAlertAction(title: "Nastavení", style: .default) { (_) -> Void in guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)")
                })
            }
        }
        let cancelAction = UIAlertAction(title: "Zrušit", style: .default, handler: nil)
        alertMapPermissionDenied.addAction(cancelAction)
        alertMapPermissionDenied.addAction(settingsAction)
        self.present(alertMapPermissionDenied, animated: true, completion: nil)
    }
    
    func fetchVyhlidkyOnMap(_ vyhlidky: Vyhlidky) {
        let data = vyhlidky.features
        for vyhlidka in data {
            let annotation = MyAnnotation(title: vyhlidka.properties.nazevstanoviste, subtitle: vyhlidka.properties.mc, coordinate: CLLocationCoordinate2D(latitude: Double(vyhlidka.geometry.coordinates.last!), longitude: Double(vyhlidka.geometry.coordinates.first!)), image: #imageLiteral(resourceName: "flag"))
            
            mapView.addAnnotation(annotation)
            mapView.showAnnotations(mapView.annotations, animated: true)
        }
    }
    
    func setUpUserLocationButton() {
        //TODO: layout them better
        let buttonMargins : CGFloat = 48
        if userTracker == nil {
            userTracker = MKUserTrackingButton(mapView: self.mapView)
            userTracker!.backgroundColor = .white
            view.addSubview(userTracker!)
            userTracker!.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin]
            setGenericShadow(subView: userTracker!)
            
            self.mapView.showsCompass = false
            compass = MKCompassButton(mapView: mapView)
            compass!.compassVisibility = .visible
            compass!.autoresizingMask = [.flexibleTopMargin, .flexibleLeftMargin]
            view.addSubview(compass!)
        }
        
        compass!.frame.origin.x = self.mapView.safeAreaInsets.left + self.mapView.frame.width - compass!.frame.size.width - buttonMargins
        compass!.frame.origin.y = self.mapView.safeAreaInsets.top + buttonMargins
        
        userTracker!.frame.origin.x = self.mapView.safeAreaInsets.left + self.mapView.frame.width - userTracker!.frame.size.width - buttonMargins
        userTracker!.frame.origin.y = self.mapView.safeAreaInsets.top + compass!.frame.height + buttonMargins + buttonMargins / 2
    }
    
    func setGenericShadow(subView:UIView) {
        subView.layer.shadowPath = UIBezierPath(rect:  subView.bounds).cgPath
        subView.layer.shadowOpacity = 0.2
        subView.layer.shadowColor = UIColor.black.cgColor
        subView.layer.shadowOffset = CGSize(width: 0, height: 0)
        subView.layer.shadowRadius = 4
        subView.layer.cornerRadius = 5
        subView.layer.masksToBounds = false
    }
        
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            //TODO: make logic for detail click - what to show?
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            //annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = #imageLiteral(resourceName: "flag")
        }
        return annotationView
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
