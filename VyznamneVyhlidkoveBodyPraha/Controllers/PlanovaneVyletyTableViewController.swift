//
//  PlanovaneVyletyTableViewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 09/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class PlanovaneVyletyTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var vyletyTableView: UITableView!
    
    var vylety = [Vylety]()
    var sortedVylety = [Vylety]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.title = "Plánované výlety"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.data(forKey: "vylety") != nil {
            if let loadedVylety = UserDefaults.standard.data(forKey: "vylety") {
                let decoded = try! JSONDecoder().decode([Vylety].self, from: loadedVylety)
                vylety = decoded
                sortedVylety = vylety.sorted()
                
                vyletyTableView.reloadData()
            }
        }
        print(vylety)
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sortedVylety.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vyletyCell", for: indexPath) as! TableViewCell
        
        cell.labelNazevVyletu.text? = sortedVylety[indexPath.row].nazevVyletu
        cell.labelNazevMista.text? = sortedVylety[indexPath.row].nazevMista
        cell.labelNazevMista.font = UIFont.boldSystemFont(ofSize: 10)
        cell.labelDatumVyletu.text? = sortedVylety[indexPath.row].datum
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        vyletyTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let nazevVyletu = UILabel()
        nazevVyletu.frame = CGRect.init(x: 20, y: 5, width: 125, height: headerView.frame.height-10)
        nazevVyletu.text = "Název výletu"
        nazevVyletu.textColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        
        let datumVyletu = UILabel()
        datumVyletu.frame = CGRect.init(x: headerView.frame.width-133, y: 5, width: 125, height: headerView.frame.height-10)
        datumVyletu.text = "Datum výletu"
        datumVyletu.textColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        datumVyletu.textAlignment = .right
        
        headerView.addSubview(nazevVyletu)
        headerView.addSubview(datumVyletu)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "vyletDetailSegue", let vc = segue.destination as? VyletDetail, let indexPath = vyletyTableView.indexPathForSelectedRow?.row {
            
            vc.idMista = sortedVylety[indexPath].id
            vc.nazevVyletu = sortedVylety[indexPath].nazevVyletu
            vc.datumVyletu = sortedVylety[indexPath].datum
            vc.souradnice = sortedVylety[indexPath].souradnice
            vc.vylety = [sortedVylety[indexPath]]
        }
    }
}
