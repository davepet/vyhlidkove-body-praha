//
//  DatabaseViewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 25/02/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

class DatabaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    @IBAction func unwindToDBVC(segue: UIStoryboardSegue) { }
    
    
    var data : Vyhlidky = Vyhlidky(type: "", name: "", features: [])
    var filteredData : Vyhlidky = Vyhlidky(type: "", name: "", features: [])
    
    var apiCall = API()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var searchActive : Bool = false
    
    var navstevy = [Navstevy]()
    
    var oblibene = [Oblibene]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Vyhledejte vyhlídkové místo..."
        definesPresentationContext = true
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.title = "Seznam míst"
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return filteredData.features.count
        } else {
            return data.features.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "vyhlidkyCell", for: indexPath) as! TableViewCell
        
        //TODO: showing favorite img
        if searchActive {
            cell.labelTitle?.text = filteredData.features[indexPath.row].properties.nazevstanoviste
            cell.labelTitle?.sizeToFit()
            if oblibene.contains(where: { filteredData.features[indexPath.row].properties.objectid == $0.idMista }) {
                cell.imageFavorite.image = UIImage(systemName: "star.fill")
            } else {
                cell.imageFavorite.image = nil
            }
        } else {
            cell.labelTitle?.text = data.features[indexPath.row].properties.nazevstanoviste
            cell.labelTitle?.sizeToFit()
            if oblibene.contains(where: { data.features[indexPath.row].properties.objectid == $0.idMista }) {
                cell.imageFavorite.image = UIImage(systemName: "star.fill")
            } else {
                cell.imageFavorite.image = nil
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue", let vc = segue.destination as? VyhlidkovaMistaDetail, let indexPath = tableView.indexPathForSelectedRow?.row {
            
            vc.idMista = data.features[indexPath].properties.objectid
            vc.nazevMista = data.features[indexPath].properties.nazevstanoviste
            vc.mcMista = data.features[indexPath].properties.mc
            vc.kuMista = data.features[indexPath].properties.ku
            vc.souradniceMista = data.features[indexPath].geometry.coordinates
        }
    }
    
    //MARK: Search bar
    func updateSearchResults(for searchController: UISearchController) {
        //TODO: ability to search not only eng chars
        let searchText = searchController.searchBar.text!.lowercased()
        
        if (searchController.searchBar.text! == "" || searchController.searchBar.text == nil) {
            searchActive = false
            self.filteredData = data
        } else if searchText.count > 2 {
            searchActive = true
            self.filteredData.features = data.features.filter { $0.properties.nazevstanoviste.lowercased().contains(searchText) }
        }
        self.tableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    //MARK: Loading data
    func loadData() {
        if UserDefaults.standard.object(forKey: "vyhlidky") != nil {
            debugPrint("file available")
            
            if let savedVyhlidka = UserDefaults.standard.object(forKey: "vyhlidky") as? Data {
                if let loadedVyhlidka = try? JSONDecoder().decode(Vyhlidky.self, from: savedVyhlidka) {
                    self.data = loadedVyhlidka
                    self.tableView.reloadData()
                }
            }
        } else {
            debugPrint("not saved and retrieved, loading from API")
            if Reachability.isConnectedToNetwork() {
                debugPrint("connection to internet is available")
                apiCall.fetchData() { (vysledneVyhlidky)  in
                    DispatchQueue.main.async {
                        self.data = vysledneVyhlidky
                        self.tableView.reloadData()
                        
                        if let encoded = try? JSONEncoder().encode(vysledneVyhlidky) {
                            UserDefaults.standard.set(encoded, forKey: "vyhlidky")
                        }
                    }
                }
            } else {
                debugPrint("not connected to internet on DatabaseViewController\nShowing OfflineViewController")
                let offlineVC = self.storyboard?.instantiateViewController(identifier: "OfflineViewController") as! OfflineViewController
                let navigationController = UINavigationController(rootViewController: offlineVC)
                navigationController.modalPresentationStyle = .fullScreen
                self.show(navigationController, sender: self)
            }
        }
        
        if UserDefaults.standard.object(forKey: "navstevy") != nil {
            if let savedNavsteva = UserDefaults.standard.object(forKey: "navstevy") as? Data {
                if let loadedNavsteva = try? JSONDecoder().decode(Navstevy.self, from: savedNavsteva) {
                    self.navstevy = [loadedNavsteva]
                    self.tableView.reloadData()
                }
            }
        } else {
            let encodedData = try? JSONEncoder().encode(navstevy)
            UserDefaults.standard.set(encodedData, forKey: "navstevy")
        }
        
        if UserDefaults.standard.object(forKey: "oblibene") != nil {
            if let savedOblibeno = UserDefaults.standard.object(forKey: "oblebene") as? Data {
                if let loadedOblibeno = try? JSONDecoder().decode(Oblibene.self, from: savedOblibeno) {
                    self.oblibene = [loadedOblibeno]
                    self.tableView.reloadData()
                }
            }
        }
    }
}
