//
//  MyAnnotation.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 26/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import MapKit

class MyAnnotation: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    var image: UIImage?
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D, image: UIImage) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.image = image
        
        super.init()
    }
}
