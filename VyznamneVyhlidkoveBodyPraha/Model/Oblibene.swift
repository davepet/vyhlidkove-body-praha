//
//  Oblbene.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 01/04/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation

struct Oblibene : Codable {
    var id : Int
    var idMista : Int
    var jeOblibeno : Bool = false
    
    init(id: Int, idMista: Int, jeOblibebo: Bool) {
        self.id = id
        self.idMista = idMista
        self.jeOblibeno = jeOblibebo
    }
}
