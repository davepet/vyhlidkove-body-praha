//
//  Vylety.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 11/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

struct Vylety : Codable/*, NSObject, NSCoding*/ {
    var id : Int
    var nazevVyletu : String
    var nazevMista : String
    var mestskaCast : String
    var kataUrad : String
    var souradnice : [Double]
    var datum : String
    
    init(id: Int, nazevVyletu: String, nazevMista : String, mestskaCast : String, kataUrad : String, souradnice : [Double], datum : String) {
        self.id = id
        self.nazevVyletu = nazevVyletu
        self.nazevMista = nazevMista
        self.mestskaCast = mestskaCast
        self.kataUrad = kataUrad
        self.souradnice = souradnice
        self.datum = datum
    }
    /*
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeInteger(forKey: "id")
        let nazevVyletu = aDecoder.decodeObject(forKey: "nazevVyletu") as! String
        let nazevMista = aDecoder.decodeObject(forKey: "nazevMista") as! String
        let mestskaCast = aDecoder.decodeObject(forKey: "mestskaCast") as! String
        let kataUrad = aDecoder.decodeObject(forKey: "kataUrad") as! String
        let souradnice = aDecoder.decodeObject(forKey: "souradnice") as! [Double]
        let datum = aDecoder.decodeObject(forKey: "datum") as! String
        
        self.init(id: id, nazevVyletu: nazevVyletu, nazevMista: nazevMista, mestskaCast: mestskaCast, kataUrad: kataUrad, souradnice: souradnice, datum: datum)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(nazevVyletu, forKey: "nazevVyletu")
        aCoder.encode(nazevMista, forKey: "nazevMista")
        aCoder.encode(mestskaCast, forKey: "mestskaCast")
        aCoder.encode(kataUrad, forKey: "kataUrad")
        aCoder.encode(souradnice, forKey: "souradnice")
        aCoder.encode(datum, forKey: "datum")
    }*/
}

extension Vylety : Comparable {
    public static func == (lhs: Vylety, rhs: Vylety) -> Bool {
        return lhs.nazevVyletu.lowercased() == rhs.nazevVyletu.lowercased()
    }
    
    public static func < (lhs: Vylety, rhs: Vylety) -> Bool {
        return lhs.nazevVyletu.lowercased() < rhs.nazevVyletu.lowercased()
    }
}
