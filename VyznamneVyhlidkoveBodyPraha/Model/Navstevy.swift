//
//  Navstevy.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 19/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation

struct Navstevy : Codable {
    var id : Int
    var idMista : Int
    var datumNavstevy : String
    
    init(id: Int, idMista: Int, datumNavstevy: String) {
        self.id = id
        self.idMista = idMista
        self.datumNavstevy = datumNavstevy
    }
}
