// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let vyhlidky = try? newJSONDecoder().decode(Vyhlidky.self, from: jsonData)

import Foundation

// MARK: - Vyhlidky
struct Vyhlidky: Codable {
    var type, name: String
    var features: [Feature]
}

// MARK: - Feature
struct Feature: Codable {
    var type: FeatureType
    var geometry: Geometry
    var properties: Properties
}

// MARK: - Geometry
struct Geometry: Codable {
    var type: GeometryType
    var coordinates: [Double]
}

enum GeometryType: String, Codable {
    case point = "Point"
}

// MARK: - Properties
struct Properties: Codable {
    var objectid, vyhbodID: Int
    var mc, ku, hodnota: String
    var vyznambodhorizont: Vyznambodhorizont
    var viditelnost, polohastanoviste, nazevstanoviste: String

    enum CodingKeys: String, CodingKey {
        case objectid = "OBJECTID"
        case vyhbodID = "VYHBOD_ID"
        case mc = "MC"
        case ku = "KU"
        case hodnota = "HODNOTA"
        case vyznambodhorizont = "VYZNAMBODHORIZONT"
        case viditelnost = "VIDITELNOST"
        case polohastanoviste = "POLOHASTANOVISTE"
        case nazevstanoviste = "NAZEVSTANOVISTE"
    }
}

enum Vyznambodhorizont: String, Codable {
    case empty = " "
    case vyznambodhorizont = "+"
}

enum FeatureType: String, Codable {
    case feature = "Feature"
}
