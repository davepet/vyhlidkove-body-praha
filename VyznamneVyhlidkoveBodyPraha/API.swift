//
//  API.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 21/02/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation

class API {
    
    func fetchData(_ completion: @escaping (Vyhlidky) -> Void) {
        let url = URL(string: "http://opendata.iprpraha.cz/CUR/URK/URK_VyznVyhlBody_b/WGS_84/URK_VyznVyhlBody_b.json")!
        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }
            
            if let data = data,
                let vyhlidkySumm = try? JSONDecoder().decode(Vyhlidky.self, from: data) {
                completion(vyhlidkySumm)
            }
        })
        task.resume()
    }
}
