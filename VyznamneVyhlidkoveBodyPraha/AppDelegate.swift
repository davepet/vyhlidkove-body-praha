//
//  AppDelegate.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 21/02/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UINavigationBar.appearance().backgroundColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        
//        UIButton.appearance().layer.cornerRadius = 4
//        UIButton.appearance().layer.shadowColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
//        UIButton.appearance().layer.shadowOffset = CGSize(width: 2, height: 2)
//        UIButton.appearance().layer.shadowRadius = 5
//        UIButton.appearance().layer.shadowOpacity = 0.5
//        UIButton.appearance().layer.masksToBounds = true
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            print("granted: (\(granted)")
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

