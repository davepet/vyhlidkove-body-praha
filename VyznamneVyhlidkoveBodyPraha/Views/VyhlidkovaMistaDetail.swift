//
//  VyhlidkovaMistaDetail.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 04/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class VyhlidkovaMistaDetail : UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapDetailView: MKMapView!
    @IBOutlet weak var labelDetailTitle: UILabel!
    @IBOutlet weak var labelDetailMC: UILabel!
    @IBOutlet weak var labelDetailKU: UILabel!
    @IBOutlet weak var labelDetailMCPopis: UILabel!
    @IBOutlet weak var labelDetailKUPopis: UILabel!
    @IBOutlet weak var labelDetailNavstivenoPopis: UILabel!
    @IBOutlet weak var labelDetailNavstiveno: UILabel!
    @IBOutlet weak var buttonDetailPridat: UIButton!
    @IBOutlet weak var buttonFavorite: UIButton!
    
    
    var idMista = Int()
    var nazevMista = String()
    var mcMista = String()
    var kuMista = String()
    var souradniceMista = [Double]()
    
    var navstevy = [Navstevy]()
    
    var oblibene = [Oblibene]()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        mapDetailView.delegate = self
        
        let annotation = MyAnnotation(title: nazevMista, subtitle: mcMista, coordinate: CLLocationCoordinate2D(latitude: Double(souradniceMista.last!), longitude: Double(souradniceMista.first!)), image: #imageLiteral(resourceName: "flag"))
        
        mapDetailView.addAnnotation(annotation)
        mapDetailView.showAnnotations(mapDetailView.annotations, animated: true)
        
        labelDetailTitle.text = nazevMista
        labelDetailTitle.numberOfLines = 0
        labelDetailTitle.font = UIFont.boldSystemFont(ofSize: 18)
        
        labelDetailMC.text = "Městská část: "
        labelDetailMCPopis.text = mcMista
        
        labelDetailKU.text = "Katastrální území: "
        labelDetailKUPopis.text = kuMista
        
        labelDetailNavstivenoPopis.text = "Datum návštěvy místa"
        labelDetailNavstivenoPopis.sizeToFit()
        
        labelDetailNavstiveno.sizeToFit()
        
        buttonDetailPridat.layer.cornerRadius = 14
        buttonDetailPridat.layer.borderWidth = 2
        buttonDetailPridat.layer.borderColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        buttonDetailPridat.layer.backgroundColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        buttonDetailPridat.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        buttonDetailPridat.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonDetailPridat.layer.shadowOffset = CGSize(width: 6, height: 6)
        buttonDetailPridat.layer.shadowRadius = 14
        buttonDetailPridat.layer.shadowOpacity = 1.0
        buttonDetailPridat.layer.masksToBounds = true
        buttonDetailPridat.sizeToFit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if UserDefaults.standard.data(forKey: "navstevy") != nil {
            if let loadedNavstevy = UserDefaults.standard.data(forKey: "navstevy") {
                let decoded = try! JSONDecoder().decode([Navstevy].self, from: loadedNavstevy)
                navstevy = decoded
            }
        }
        
        if UserDefaults.standard.data(forKey: "oblibene") != nil {
            if let loadedOblibene = UserDefaults.standard.data(forKey: "oblibene") {
                let decoded = try! JSONDecoder().decode([Oblibene].self, from: loadedOblibene)
                oblibene = decoded
            }
        }
        
        for navsteva in navstevy {
            if navstevy.contains(where: { idMista == $0.idMista }) {
                labelDetailNavstiveno.text = navsteva.datumNavstevy
            } else {
                labelDetailNavstiveno.text = "Doposud nenavštíveno"
            }
        }
        

        
        for oblibeny in oblibene {
            if oblibene.contains(where: { idMista == $0.idMista }) {
                print(oblibeny)
                if oblibeny.jeOblibeno {
                    buttonFavorite.setImage(UIImage(systemName: "star.fill"), for: .normal)
                    buttonFavorite.tintColor = .systemYellow
                } else {
                    buttonFavorite.setImage(UIImage(systemName: "star"), for: .normal)
                    buttonFavorite.tintColor = .systemYellow
                }
            }
        }
    }
    
    @IBAction func buttonPridatClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(identifier: "vyletPopUp") as! VyletPopUp
        popupVC.modalPresentationStyle = .fullScreen
        let navigationController = UINavigationController.init(rootViewController: popupVC)
        
        popupVC.idMista = idMista
        popupVC.titleMista = nazevMista
        popupVC.mcMista = mcMista
        popupVC.kuMista = kuMista
        popupVC.souradniceMista = souradniceMista
            
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func buttonFavoriteClicked(_ sender: UIButton) {
        if oblibene.contains(where: { idMista == $0.idMista}) {
            oblibene.removeAll(where: { $0.idMista == idMista })
            let encodedVylet = try! JSONEncoder().encode(oblibene)
            UserDefaults.standard.set(encodedVylet, forKey: "oblibene")
            buttonFavorite.setImage(UIImage(systemName: "star"), for: .normal)
        } else {
            oblibene.append(Oblibene(id: oblibene.count + 1, idMista: idMista, jeOblibebo: true))
            let encodedOblibene = try! JSONEncoder().encode(oblibene)
            UserDefaults.standard.set(encodedOblibene, forKey: "oblibene")
            buttonFavorite.setImage(UIImage(systemName: "star.fill"), for: .normal)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            //TODO: make logic for detail click - what to show?
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            //annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            
            annotationView.canShowCallout = true
            annotationView.image = #imageLiteral(resourceName: "flag")
        }
        return annotationView
    }
}
