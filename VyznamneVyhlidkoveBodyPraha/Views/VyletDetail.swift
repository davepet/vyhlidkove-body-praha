//
//  VyletDetail.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 12/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Contacts
import EventKit

class VyletDetail: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var labelCilovaDestinace: UILabel!
    @IBOutlet weak var labelNazevMista: UILabel!
    @IBOutlet weak var labelDatumPopis: UILabel!
    @IBOutlet weak var labelDatumVyletu: UILabel!
    @IBOutlet weak var mapVylet: MKMapView!
    @IBOutlet weak var buttonKalendar: UIButton!
    @IBOutlet weak var buttonNavstiveno: UIButton!
    
    var idMista = Int()
    var nazevVyletu = String()
    var souradnice = [Double]()
    var datumVyletu = String()
    var vylety = [Vylety]()
    
    var vyletyAktualizace = [Vylety]()
    
    var navstevy = [Navstevy]()
    
    let store = EKEventStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelCilovaDestinace.text = "Místo výletu: "
        labelDatumPopis.text = "Datum výletu: "
        
        mapVylet.delegate = self
        
        buttonKalendar.layer.cornerRadius = 14
        buttonKalendar.layer.borderWidth = 2
        buttonKalendar.layer.borderColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        buttonKalendar.layer.backgroundColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        buttonKalendar.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        buttonKalendar.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonKalendar.layer.shadowOffset = CGSize(width: 6, height: 6)
        buttonKalendar.layer.shadowRadius = 14
        buttonKalendar.layer.shadowOpacity = 1.0
        buttonKalendar.layer.masksToBounds = true
        
        buttonNavstiveno.layer.cornerRadius = 14
        buttonNavstiveno.layer.borderWidth = 2
        buttonNavstiveno.layer.borderColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        buttonNavstiveno.layer.backgroundColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        buttonNavstiveno.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        buttonNavstiveno.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonNavstiveno.layer.shadowOffset = CGSize(width: 6, height: 6)
        buttonNavstiveno.layer.shadowRadius = 14
        buttonNavstiveno.layer.shadowOpacity = 1.0
        buttonNavstiveno.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            for title in vylety {
                navigationItem.title = title.nazevVyletu
            }
        }
        
        if UserDefaults.standard.data(forKey: "navstevy") != nil {
            if let loadedNavstevy = UserDefaults.standard.data(forKey: "navstevy") {
                let decoded = try! JSONDecoder().decode([Navstevy].self, from: loadedNavstevy)
                navstevy = decoded
            }
        }
        
        if UserDefaults.standard.data(forKey: "vylety") != nil {
            if let loadedVylety = UserDefaults.standard.data(forKey: "vylety") {
                let decoded = try! JSONDecoder().decode([Vylety].self, from: loadedVylety)
                vyletyAktualizace = decoded
            }
        }
        
        for vylet in vylety {
            labelNazevMista.text = vylet.nazevMista
            labelNazevMista.sizeToFit()
            labelDatumVyletu.text = vylet.datum
            labelDatumVyletu.sizeToFit()
            
            let annotation = MyAnnotation(title: nazevVyletu, subtitle: vylet.nazevMista, coordinate: CLLocationCoordinate2D(latitude: Double(souradnice.last!), longitude: Double(souradnice.first!)), image: #imageLiteral(resourceName: "flag"))
            
            mapVylet.addAnnotation(annotation)
            mapVylet.showAnnotations(mapVylet.annotations, animated: true)
        }
    }
    
    @IBAction func buttonAddToCalendar(_ sender: UIButton) {
        let alert = UIAlertController(title: "Uložení do kalendáře", message: "Chcete si uložit datum výletu do vašeho kalendáře?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Zrušit", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Přidat", style: .default, handler: { (action: UIAlertAction!) in
            self.addEvent()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func buttonNavstivenoClicked(_ sender: UIButton) {
        //TODO: user notification one day before an event
        if navstevy.contains(where: { idMista == $0.idMista}) {
            print("not added")
        } else {
            navstevy.append(Navstevy(id: navstevy.count + 1, idMista: idMista, datumNavstevy: datumVyletu))
            let encodedNavsteva = try! JSONEncoder().encode(navstevy)
            UserDefaults.standard.set(encodedNavsteva, forKey: "navstevy")
            
            //TODO: reusable component for alert controller
            
            if navstevy.count == 1 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste první významné vyhlídkové místo v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 5 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste pět významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 10 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste deset významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 25 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste pětadvacet významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 50 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste padesát významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 100 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste rovných sto významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 200 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste dvě stě významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 300 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste tři sta významných vyhlídkových míst v Praze, gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else if navstevy.count == 323 {
                let alert = UIAlertController(title: "Získano nové ocenění!", message: "Navštívili jste všechny významná vyhlídková místa v Praze! Gratulujeme!", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Super!", style: .default, handler: { (action: UIAlertAction) in
                    alert.dismiss(animated: true, completion: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                }))
                present(alert, animated: true, completion: nil)
            } else {
                _ = navigationController?.popViewController(animated: true)
            }
        }
        
        vyletyAktualizace.removeAll(where: { $0.id == idMista && $0.nazevVyletu == nazevVyletu })
        let encodedVylet = try! JSONEncoder().encode(vyletyAktualizace)
        UserDefaults.standard.set(encodedVylet, forKey: "vylety")
        
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !(annotation is MKUserLocation) else { return nil }
        
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            let smallSquare = CGSize(width: 30, height: 30)
            let button = UIButton(frame: CGRect(origin: CGPoint.zero, size: smallSquare))
            button.setBackgroundImage(UIImage(systemName: "paperplane"), for: .normal)
            button.addTarget(self, action: #selector(showPopUp), for: .touchUpInside)
            
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            //annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView?.leftCalloutAccessoryView = button
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = true
            annotationView.image = #imageLiteral(resourceName: "flag")
        }
        
        return annotationView
    }
    
    @objc func showPopUp() {
        let alert = UIAlertController(title: "Navigace na místo", message: "Chystáte se otevřít aplikaci Mapy a zobrazit ideální trasu na zvolené místo.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Zrušit", style: .default, handler: { (action: UIAlertAction!) in
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Otevřít", style: .default, handler: { (action: UIAlertAction!) in
            self.getDirections()
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func getDirections() {
        var souradnice : [Double] = []
        for vylet in vylety {
            souradnice = vylet.souradnice
        }
        
        let coordinates = CLLocationCoordinate2D(latitude: souradnice.last!, longitude: souradnice.first!)
        
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = nazevVyletu
        
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeTransit]
        mapItem.openInMaps(launchOptions: launchOptions)
    }
    
    private func generateEvent() -> EKEvent {
        let newEvent = EKEvent(eventStore: store)
        newEvent.calendar = store.defaultCalendarForNewEvents
        newEvent.title = nazevVyletu
        
        var location = ""
        
        for vylet in vylety {
            newEvent.notes = vylet.nazevMista
            location = vylet.mestskaCast
        }
        
        newEvent.location = location
        
        let coordinates = CLLocation(latitude: souradnice.last!, longitude: souradnice.first!)
        let structuredLocation = EKStructuredLocation(title: location)
        structuredLocation.geoLocation = coordinates
        newEvent.structuredLocation = structuredLocation
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        guard let date = dateFormatter.date(from: datumVyletu) else { fatalError() }
        
        newEvent.startDate = date
        newEvent.endDate = date
        newEvent.isAllDay = true
        
        //        let alarm = EKAlarm(relativeOffset: -86400)
        //        newEvent.addAlarm(alarm)
        
        return newEvent
    }
    
    private func addEvent() {
        let eventToAdd = generateEvent()
        
        switch EKEventStore.authorizationStatus(for: .event) {
        case .authorized:
            if !eventAlreadyExists(event: eventToAdd) {
                do {
                    try self.store.save(eventToAdd, span: .thisEvent)
                    let alertSuccess = UIAlertController(title: "Úspěšné přidání", message: "Úspěšně jste přidali tento výlet do vašeho kalendáře.", preferredStyle: .alert)
                    self.present(alertSuccess, animated: true, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                        alertSuccess.dismiss(animated: true, completion: nil)
                    }
                } catch {
                    debugPrint("error \(error.localizedDescription)")
                }
            } else {
                let alertUnsuccess = UIAlertController(title: "Neúspěšné přidání", message: "Daný výlet v kalendáři již existuje.", preferredStyle: .alert)
                self.present(alertUnsuccess, animated: true, completion: nil)
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                    alertUnsuccess.dismiss(animated: true, completion: nil)
                }
            }
        case .denied:
            let alertCalendarPermissionDenied = UIAlertController(title: "Není povolený přístup ke kalendáři", message: "Pro ukládání výletů do kalendáře povolte přístup ke kalendáři v nastavení.", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Nastavení", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }
            let cancelAction = UIAlertAction(title: "Zrušit", style: .default, handler: nil)
            alertCalendarPermissionDenied.addAction(settingsAction)
            alertCalendarPermissionDenied.addAction(cancelAction)
            self.present(alertCalendarPermissionDenied, animated: true, completion: nil)
        case .notDetermined:
            store.requestAccess(to: .event, completion:
                {(granted: Bool, error: Error?) -> Void in
                    if granted {
                        if !self.eventAlreadyExists(event: eventToAdd) {
                            do {
                                try self.store.save(eventToAdd, span: .thisEvent)
                                let alertSuccess = UIAlertController(title: "Úspěšné přidání", message: "Úspěšně jste přidali tento výlet do vašeho kalendáře.", preferredStyle: .alert)
                                self.present(alertSuccess, animated: true, completion: nil)
                                
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                                    alertSuccess.dismiss(animated: true, completion: nil)
                                }
                            } catch {
                                debugPrint("error \(error.localizedDescription)")
                            }
                        } else {
                            let alertUnsuccess = UIAlertController(title: "Neúspěšné přidání", message: "Daný výlet v kalendáři již existuje.", preferredStyle: .alert)
                            self.present(alertUnsuccess, animated: true, completion: nil)
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
                                alertUnsuccess.dismiss(animated: true, completion: nil)
                            }
                        }
                    } else {
                        let alertCalendarPermissionDenied = UIAlertController(title: "Není povolený přístup ke kalendáři", message: "Pro ukládání výletů do kalendáře povolte přístup ke kalendáři v nastavení.", preferredStyle: .alert)
                        let settingsAction = UIAlertAction(title: "Nastavení", style: .default) { (_) -> Void in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                    print("Settings opened: \(success)")
                                })
                            }
                        }
                        let cancelAction = UIAlertAction(title: "Zrušit", style: .default, handler: nil)
                        alertCalendarPermissionDenied.addAction(cancelAction)
                        alertCalendarPermissionDenied.addAction(settingsAction)
                        self.present(alertCalendarPermissionDenied, animated: true, completion: nil)
                    }
            })
            print("Not Determined")
        default:
            print("Case Default")
        }
        
        
    }
    
    private func eventAlreadyExists(event eventToAdd: EKEvent) -> Bool {
        let predicate = store.predicateForEvents(withStart: eventToAdd.startDate, end: eventToAdd.endDate, calendars: nil)
        let existingEvents = store.events(matching: predicate)
        
        let eventAlreadyExists = existingEvents.contains { (event) -> Bool in
            return eventToAdd.title == event.title && event.startDate == eventToAdd.startDate && event.endDate == eventToAdd.endDate
        }
        return eventAlreadyExists
    }
}
