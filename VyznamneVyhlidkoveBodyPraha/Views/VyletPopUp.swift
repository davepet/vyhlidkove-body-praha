//
//  VyletPopUp.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 09/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

class VyletPopUp : UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var labelNazevVyletu: UILabel!
    @IBOutlet weak var textFieldNazev: UITextField!
    @IBOutlet weak var labelDatumVyletu: UILabel!
    @IBOutlet weak var datePickerVylet: UIDatePicker!
    @IBOutlet weak var buttonNaplanovat: UIButton!
    @IBOutlet weak var labelNazevValidation: UILabel!
    
    var vylety = [Vylety]()
    
    var idMista = Int()
    var titleMista = String()
    var mcMista = String()
    var kuMista = String()
    var souradniceMista = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = titleMista
        let backButton = UIBarButtonItem(title: "Zpět", style: UIBarButtonItem.Style.plain, target: self, action: nil)
        navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        labelNazevVyletu.text = "Zadejte název pro váš výlet"
        labelNazevVyletu.sizeToFit()
        labelNazevVyletu.numberOfLines = 1
        
        textFieldNazev.delegate = self
        textFieldNazev.placeholder = "Název..."
        
        labelNazevValidation.text = "Vyplňte prosím název pro váš výlet"
        labelNazevValidation.font = UIFont(name: labelNazevValidation.font.fontName, size: 12)
        labelNazevValidation.textColor = UIColor.red
        labelNazevValidation.sizeToFit()
        labelNazevValidation.numberOfLines = 1
        labelNazevValidation.isHidden = true
        
        labelDatumVyletu.text = "Zvolte si datum vašeho výletu"
        labelDatumVyletu.sizeToFit()
        
        datePickerVylet.datePickerMode = .date
        datePickerVylet.minimumDate = Date()
        
        buttonNaplanovat.layer.cornerRadius = 14
        buttonNaplanovat.layer.borderWidth = 2
        buttonNaplanovat.layer.borderColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        buttonNaplanovat.layer.backgroundColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        buttonNaplanovat.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        buttonNaplanovat.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonNaplanovat.layer.shadowOffset = CGSize(width: 6, height: 6)
        buttonNaplanovat.layer.shadowRadius = 14
        buttonNaplanovat.layer.shadowOpacity = 1.0
        buttonNaplanovat.layer.masksToBounds = true
        buttonNaplanovat.setTitle("Naplánovat výlet", for: .normal)
        buttonNaplanovat.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if UserDefaults.standard.data(forKey: "vylety") != nil {
            if let loadedVylety = UserDefaults.standard.data(forKey: "vylety") {
                let decoded = try! JSONDecoder().decode([Vylety].self, from: loadedVylety)
                vylety = decoded
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textFieldNazev.text == "" {
            textFieldNazev.placeholder = "Název..."
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldNazev.placeholder = ""
        textFieldNazev = textField
    }
    
    @IBAction func buttonNaplanovatClicked(_ sender: UIButton) {
        let formatter = DateFormatter()
        formatter.calendar = datePickerVylet.calendar
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        let dateString = formatter.string(from: datePickerVylet.date)
        
        if textFieldNazev.text == "" {
            textFieldNazev.layer.borderWidth = 1.0
            textFieldNazev.layer.borderColor = UIColor.red.cgColor
            labelNazevValidation.isHidden = false
        } else {
            textFieldNazev.layer.borderWidth = 1.0
            textFieldNazev.layer.borderColor = UIColor.green.cgColor
            labelNazevValidation.isHidden = true
            
            let newVylet = Vylety(id: idMista, nazevVyletu: String(textFieldNazev.text!), nazevMista: titleMista, mestskaCast: mcMista, kataUrad: kuMista, souradnice: souradniceMista, datum: dateString)
            
            vylety.append(newVylet)
            
            let encodedData = try! JSONEncoder().encode(vylety)
            UserDefaults.standard.set(encodedData, forKey: "vylety")
            successfullAdded()
        }
    }
    
    func successfullAdded() {
        let alert = UIAlertController(title: "Úspěšné přidání záznamu", message: "Přidali jste do seznamu plánovaných výletů nový výlet - \(textFieldNazev.text!)", preferredStyle: .alert)
        
        self.present(alert, animated: true, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.performSegue(withIdentifier: "unwindSegueToDBVC", sender: self)
        })
    }
}
