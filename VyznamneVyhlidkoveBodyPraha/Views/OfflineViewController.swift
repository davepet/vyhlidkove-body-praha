//
//  OfflineViewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 10/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

class OfflineViewController: UIViewController {
    
    @IBOutlet weak var labelInternetConnection: UILabel!
    @IBOutlet weak var imageInternet: UIImageView!
    @IBOutlet weak var buttonRetry: UIButton!
    
    override func viewDidLoad() {
        labelInternetConnection.text = "Není aktivní připojení k internetu.\nPřipojte své zařízení k internetu a zkuste to znovu."
        labelInternetConnection.sizeToFit()
        labelInternetConnection.numberOfLines = 0
        buttonRetry.setTitle("Zkusit znovu", for: .normal)
        buttonRetry.titleLabel?.sizeToFit()
        
        buttonRetry.layer.cornerRadius = 14
        buttonRetry.layer.borderWidth = 2
        buttonRetry.layer.borderColor = #colorLiteral(red: 0.4747117162, green: 0.3184991479, blue: 0.09865670651, alpha: 1)
        buttonRetry.layer.backgroundColor = #colorLiteral(red: 0.01284001768, green: 0.6452069879, blue: 0.03052881733, alpha: 1)
        buttonRetry.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        buttonRetry.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        buttonRetry.layer.shadowOffset = CGSize(width: 6, height: 6)
        buttonRetry.layer.shadowRadius = 14
        buttonRetry.layer.shadowOpacity = 1.0
        buttonRetry.layer.masksToBounds = true
    }
    
    @IBAction func buttonRetryClicked(_ sender: UIButton) {
        if UserDefaults.standard.object(forKey: "vyhlidky") != nil {
            let databaseVC = self.storyboard?.instantiateViewController(identifier: "DatabaseViewController") as! DatabaseViewController
            let navigationController = UINavigationController(rootViewController: databaseVC)
            navigationController.modalPresentationStyle = .fullScreen
            self.present(navigationController, animated: true, completion: nil)
        } else {
            if Reachability.isConnectedToNetwork() {
                let databaseVC = self.storyboard?.instantiateViewController(identifier: "DatabaseViewController") as! DatabaseViewController
                let navigationController = UINavigationController(rootViewController: databaseVC)
                navigationController.modalPresentationStyle = .fullScreen
                self.present(navigationController, animated: true, completion: nil)
            }
        }
    }
}
