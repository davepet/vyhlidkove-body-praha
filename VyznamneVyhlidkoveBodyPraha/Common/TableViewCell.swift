//
//  TableViewCell.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 02/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//

import Foundation
import UIKit

class TableViewCell : UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageFavorite: UIImageView!
    
    
    @IBOutlet weak var labelNazevVyletu: UILabel!
    @IBOutlet weak var labelDatumVyletu: UILabel!
    @IBOutlet weak var labelNazevMista: UILabel!
    
}
