//
//  Ext-UIVIewController.swift
//  VyznamneVyhlidkoveBodyPraha
//
//  Created by David Petrina on 09/03/2020.
//  Copyright © 2020 David Petrina. All rights reserved.
//
import UIKit
import Foundation

extension UIViewController {
    
    func saveObject(fileName: String, object: Any) -> Bool {
        let filePath = self.getDirectoryPath().appendingPathComponent(fileName)
        
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false)
            try data.write(to: filePath)
            return true
        } catch {
            print("some error occured: \(error.localizedDescription)")
        }
        return false
    }
    
    func loadObject(fileName: String) -> Any? {
        let filePath = self.getDirectoryPath().appendingPathComponent(fileName)
        
        do {
            let data = try Data(contentsOf: filePath)
            let object = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data)
            return object
        } catch {
            print("some error occured: \(error.localizedDescription)")
        }
        return nil
    }
    
    func getDirectoryPath() -> URL {
        let arrayPaths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return arrayPaths[0]
    }
}
